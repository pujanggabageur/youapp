import { Injectable, NotFoundException } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { UserDoc } from 'src/schema/user.schema'
import { IUserDoc } from 'src/interface/user.interface'
import { CreateUserDto } from 'src/dto/user/create.dto'
import { UpdateUserDto } from '../dto/user/update.dto'
import China from '../helper/china'
import { daysBetween, formatReadDate, intVal } from '../helper/math'
import { elements, horoscopes, zodiacs } from '../helper/collections'

@Injectable()
export class UserService {
  constructor(
    @InjectModel(UserDoc.name)
    private userModel:Model<IUserDoc>
  ) {}

  async createUser(createUserDto: CreateUserDto): Promise<IUserDoc> {
    const data = await new this.userModel(createUserDto)
    return data.save()
  }

  async updateProfile(userId: string, updateUserDto: UpdateUserDto): Promise<IUserDoc> {
    const { profile, interests } = updateUserDto
    let horoscope: string = ''
    let zodiac: string = ''
    if (profile) {
      const { birthDate } = profile
      if (birthDate) {
        let date = new Date(birthDate)
        let year = date.getFullYear()
        let month = date.getMonth() + 1
        let hMonth = date.getMonth()
        let day = date.getDate()
        if (year && month && day) {
          const days = [20, 19, 21, 20, 21, 22, 23, 23, 23, 24, 22, 22]
          if (hMonth == 0 && day < days[0]){
            hMonth = 11
          } else if (day < days[hMonth]){
            hMonth--
          }
          horoscope = horoscopes[hMonth]

          const china = new China()
          const imlekDate = china.Imlek(year)
          let imlekYear = china.Tahun(year)
          let gregYear = year

          const remDays = daysBetween(imlekDate, formatReadDate(year, month, day))
          if (remDays < 0) {
            imlekYear -= 1
            gregYear -= 1
          }
          const shioIndex = imlekYear % 12
          const elementIndex = intVal(((gregYear - 4) % 10) / 2)
          zodiac = `${elements[elementIndex]} ${zodiacs[shioIndex]}`
        }

        profile['zodiac'] = zodiac
        profile['horoscope'] = horoscope
        updateUserDto['profile'] = profile
      }
    }
    const data = await this.userModel.findByIdAndUpdate(userId, updateUserDto, { new: true })
    if (!data) {
      throw new NotFoundException(`User #${userId} not found`)
    }
    return data
  }

  async getUser(email: string): Promise<IUserDoc> {
    let data = await this.userModel.findOne( { email } ).exec()
    if (!data) {
      data = await this.userModel.findOne( { userName: email } ).exec()
      if (!data) {
        throw new NotFoundException('User data not found!')
      }
    }
    return data
  }

  async getUserById(userId: string): Promise<IUserDoc> {
    const data = await this.userModel.findById(userId).exec()
    if (!data) {
      throw new NotFoundException(`User #${userId} not found`)
    }
    return data
  }

}
