import { Injectable } from '@nestjs/common'
import { UserService } from './user.service'
import { IUserDoc } from '../interface/user.interface'
import * as bcrypt from 'bcrypt'
import { JwtService } from '@nestjs/jwt'
import { CreateUserDto } from '../dto/user/create.dto'
import { InjectModel } from '@nestjs/mongoose'
import { UserDoc } from '../schema/user.schema'
import { Model } from 'mongoose'
import { UpdateUserDto } from '../dto/user/update.dto'

@Injectable()
export class AuthService {
  constructor(
    @InjectModel(UserDoc.name)
    private userModel:Model<IUserDoc>,
    private readonly userService: UserService,
    private jwtTokenService: JwtService
  ) {}

  async validateUser(
    email: string,
    password: string,
  ): Promise<any> {
    let user: IUserDoc = await this.userService.getUser(email)
    const match = await bcrypt.compare(password, user.password)
    return match ? user : undefined
  }

  async loginCredentials(user: any) {
    const payload = { email: user.email, userName: user.userName, password: user.password }
    return {
      access_token: this.jwtTokenService.sign(payload),
      // ...payload
    }
  }

  async register(createUserDto: CreateUserDto): Promise<IUserDoc> {
    return await this.userService.createUser(createUserDto)
  }

  async getProfile(userId: string): Promise<IUserDoc> {
    return await this.userService.getUserById(userId)
  }

  async updateProfile(userId: string, updateUserDto: UpdateUserDto): Promise<IUserDoc> {
    return await this.userService.updateProfile(userId, updateUserDto)
  }
}
