import { Body, Controller, Get, HttpStatus, Post, Put, Req, Request, Res, UseGuards } from '@nestjs/common';
import { AuthService } from '../service/auth.service';
import { LocalAuthGuard } from '../guard/local-auth';
import { AuthenticatedGuard } from '../guard/authenticated.guard';
import { CreateUserDto } from '../dto/user/create.dto';
import * as bcrypt from 'bcrypt';
import { UpdateUserDto } from '../dto/user/update.dto';
import { ProfileDto } from '../dto/user/profile.dto';

@Controller('')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('login')
  async login(
    @Res() response: any,
    @Req() req: any,
  ) {
    const data = await this.authService.loginCredentials(req.user)
    return response.status(HttpStatus.OK).json({
      message: 'User has been log in successfully',
      data,
    })
  }

  @Post('register')
  async register(
    @Res() response: any,
    @Body() createUserDto: CreateUserDto
  ) {
    try {
      const salt = await bcrypt.genSalt()
      createUserDto.password = await bcrypt.hash(createUserDto.password, salt)
      const data = await this.authService.register(createUserDto)
      return response.status(HttpStatus.CREATED).json({
        message: 'User has been created successfully',
        data,
      })
    } catch (err) {
      return response.status(HttpStatus.BAD_REQUEST).json({
        statusCode: 400,
        message: 'Error: User not created!',
        error: 'Bad Request'
      })
    }
  }

  @Get('logout')
  logout(
    @Request() req: any
  ): any {
    req.session.destroy()
    return { message: 'The user session has ended' }
  }

  @UseGuards(AuthenticatedGuard)
  @Get('profile')
  async getProfile(
    @Res() response: any,
    @Request() req: any
  ): Promise<any> {
    try {
      const userId = req.user._id
      const data = await this.authService.getProfile(userId)
      return response.status(HttpStatus.OK).json({
        message: 'User found',
        data,
      })
    } catch (err) {
      return response.status(err.status).json(err.response)
    }
  }

  @UseGuards(AuthenticatedGuard)
  @Put('profile')
  async updateProfile(
    @Res() response: any,
    @Request() req: any,
    @Body() updateUserDto: UpdateUserDto
  ): Promise<any> {
    try {
      const userId = req.user._id
      const data = await this.authService.updateProfile(userId, updateUserDto)
      return response.status(HttpStatus.OK).json({
        message: 'User has been successfully updated',
        data,
      })
    } catch (err) {
      return response.status(err.status).json(err.response)
    }
  }

}
