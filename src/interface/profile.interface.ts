import { Document } from 'mongoose'

export interface IProfileDoc extends Document {
  readonly about: string
  readonly displayName: string
  readonly gender: number
  readonly birthDate: Date
  readonly horoscope: string
  readonly zodiac: string
  readonly height: number
  readonly weight: number
}
