import { Document } from 'mongoose'

export interface IInterestDoc extends Document {
  readonly code: string
  readonly name: string
}
