import { Document } from 'mongoose'
import { IProfileDoc } from './profile.interface'
import { IInterestDoc } from './interest.interface'

export interface IUserDoc extends Document {
  readonly userName: string
  readonly email: string
  readonly password: string
  readonly profile: IProfileDoc
  readonly interests: IInterestDoc[]
}
