export const horoscopes = [
  'Aquarius (Water Bearer)',
  'Pisces (Fish)',
  'Aries (Ram)',
  'Taurus (Bull)',
  'Gemini (Twins)',
  'Cancer (Crab)',
  'Leo (Lion)',
  'Virgo (Virgin)',
  'Libra (Balance)',
  'Scorpius (Scorpion)',
  'Sagittarius (Archer)',
  'Capricornus (Goat)',
]

export const zodiacs = [
  'Rooster',
  'Dog',
  'Boar',
  'Rat',
  'Ox',
  'Tiger',
  'Rabbit',
  'Dragon',
  'Snake',
  'Horse',
  'Goat',
  'Monkey'
]

export const elements = [
  'Wood',
  'Fire',
  'Earth',
  'Metal',
  'Water'
]
