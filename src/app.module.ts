import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { ConfigModule } from '@nestjs/config'
import { MongooseModule } from '@nestjs/mongoose'
import { AuthModule } from './module/auth.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    /*
    MongooseModule.forRoot('mongodb://localhost:27017',{
      dbName: process.env.DB_NAME || 'youapp'
    }),
    */
    MongooseModule.forRoot(`mongodb+srv://${process.env.DB_HOST}/`,
      {
        dbName: process.env.DB_NAME || 'youapp',
        user: process.env.DB_USER,
        pass: process.env.DB_PASSWORD,
        w: 'majority',
        retryWrites: true
    }),
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
