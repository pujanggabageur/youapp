import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document, ObjectId } from 'mongoose'
import { Transform } from 'class-transformer'

export type InterestDocument = InterestDoc & Document

@Schema()
export class InterestDoc {
  @Transform(({ value }) => value.toString())
  _id: ObjectId

  @Prop()
  code: string

  @Prop()
  name: string
}

export const InterestDocSchema = SchemaFactory.createForClass(InterestDoc)
