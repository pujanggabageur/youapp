import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document, ObjectId } from 'mongoose'
import { Transform } from 'class-transformer'

export type ProfileDocument = ProfileDoc & Document

@Schema()
export class ProfileDoc {
  @Transform(({ value }) => value.toString())
  _id: ObjectId

  @Prop()
  about: string

  @Prop()
  displayName: string

  @Prop()
  horoscope: string

  @Prop()
  zodiac: string

  @Prop()
  gender: number

  @Prop()
  birthDate: Date

  @Prop()
  height: number

  @Prop()
  weight: number

}

export const ProfileDocSchema = SchemaFactory.createForClass(ProfileDoc)
