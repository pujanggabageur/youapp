import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document, ObjectId } from 'mongoose'
import { Exclude, Transform, Type } from 'class-transformer'
import { ProfileDoc, ProfileDocSchema } from './profile.schema'
import { InterestDoc, InterestDocSchema } from './interest.schema';

export type UserDocument = UserDoc & Document

@Schema()
export class UserDoc {
  @Transform(({ value }) => value.toString())
  _id: ObjectId

  @Prop({
    required: true,
    unique: true
  })
  userName: string

  @Prop({
    required: true,
    unique: true
  })
  email: string

  @Prop()
  @Exclude()
  password: string

  @Prop({ type: ProfileDocSchema })
  @Type(() => ProfileDoc)
  profile: ProfileDoc

  @Prop([{ type: InterestDocSchema }])
  @Type(() => InterestDoc)
  interests: InterestDoc[]
}

export const UserDocSchema = SchemaFactory.createForClass(UserDoc)
