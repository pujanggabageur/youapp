import { IsArray, IsEmail, IsNotEmpty, IsObject, IsOptional, IsString, MaxLength, MinLength } from 'class-validator';
import { ProfileDto } from './profile.dto'
import { InterestDto } from './interest.dto'

export class CreateUserDto {
  @IsString()
  @IsNotEmpty()
  @MinLength(6)
  userName: string

  @IsString()
  @IsNotEmpty()
  password: string

  @IsNotEmpty()
  @MaxLength(50)
  @IsEmail()
  email: string

  @IsOptional()
  @IsObject()
  profile: ProfileDto

  @IsOptional()
  @IsArray()
  interests: InterestDto[]
}
