import { IsArray, IsObject, IsOptional } from 'class-validator';
import { ProfileDto } from './profile.dto'
import { InterestDto } from './interest.dto'

export class UpdateUserDto {
  @IsOptional()
  @IsObject()
  profile: ProfileDto

  @IsOptional()
  @IsArray()
  interests: InterestDto[]
}
