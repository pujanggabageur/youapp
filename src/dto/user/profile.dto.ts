import { IsDate, IsEmpty, IsNumber, IsOptional, IsString } from 'class-validator'
import { Type } from 'class-transformer'

export class ProfileDto {
  @IsOptional()
  @IsString()
  about: string

  @IsOptional()
  @IsString()
  displayName: string

  @IsOptional()
  @IsNumber()
  gender: number

  @IsOptional()
  @Type(() => Date)
  @IsDate()
  birthDate: Date

  @IsOptional()
  @IsNumber()
  height: number

  @IsOptional()
  @IsNumber()
  weight: number

  @IsOptional()
  @IsString()
  horoscope: string

  @IsOptional()
  @IsString()
  zodiac: string
}
