import { IsString } from 'class-validator'

export class InterestDto {
  @IsString()
  code: string

  @IsString()
  name: string
}
