# Base image
FROM node:16.20.0-alpine

# Create app directory
WORKDIR /usr/src/app

# Creates ENV
ENV ACCESS_SECRET=QUNDRVNT
ENV SESSION_SECRET=U0VTU0lPTg
ENV JWT_EXPIRE=1d
ENV DB_NAME=youapp
ENV DB_USER=pujanggabageur
ENV DB_PASSWORD=llEJQbEHMZMwjkjC
ENV DB_HOST=pijanggabageur.9nj3nx1.mongodb.net

# Copy package.json
COPY package.json ./

# Install pnpm
RUN npm install -g pnpm

# Install app dependencies
RUN pnpm install --production=false --shamefully-hoist

# Bundle app source
COPY . .

# Creates a "dist" folder with the production build
RUN pnpm build

# Expose port 3000
EXPOSE 3000

# Start the server using the production build
CMD [ "node", "dist/main.js" ]
